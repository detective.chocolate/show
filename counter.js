function Counter () {
    var counter = 0;
    return {
        increment: function (){
            counter++;
            return counter;},
        decrement: function (){
            counter--;
            return counter;},
        getValue: function () {
            return counter;}
    };
};