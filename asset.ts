import { ScenarioDBInterface, SecureDBInterface, AssetDBInterface } from '../../db/interface';
import { ApiHelper } from 'typescript-express-openapi';
import translateVisualToDataScenario from '../../scenario';
import { logException, Topic, SubTopic, Severity } from '../../log';


export default function addProtectedAssetsMethods(apiHelper: ApiHelper, db: ScenarioDBInterface & SecureDBInterface & AssetDBInterface) {

    apiHelper.add('/api/protected/api/set_asset', 'post', {
        example: { 'mcn-account-id': 0, scenario_id: 0, element_id: 'some_element_id', file: 'file' },
        body_params: [],
        header_params: [{ name: 'mcn-account-id', type: 'number', required: true, description: 'Номер ЛС' }],
        query_params: [{ name: 'scenario_id', type: 'number', required: true, description: 'Номер сценария к которому будет прикреплена статика' },
            { name: 'element_id', type: 'string', required: true, description: 'Идентификатор элемента сценария, к которому будет прикреплена статика' }],
        is_file_upload: true,
        checks: []
    }, {
        response: {
            ok: { '$ref': '#/components/property/ok' },
        },
        description: 'Статика прикрепляется к конкретному узлу сценария.',
        summary: 'Создать статику', tags: ['asset']
    }, async (req, res) => {
        try {
            if (!await db.SecureDataService.checkOwnershipScenario(req['mcn-account-id'], req.scenario_id)) {
                return res.json({ ok: false, error: 'auth' });
            }

            let scenario_visual = JSON.parse(await db.ScenarioDataService.getVisualScenario(req.scenario_id));
            if (!Array.isArray(scenario_visual)) {
                return res.json({ ok: false, error: 'bad_scenario' });
            }
            let found = false;
            let asset_id;
            for (let i in scenario_visual) {
                const item = scenario_visual[i];
                if (item.id == req.element_id) {
                    //@ts-ignore
                    let buffer: any = req.file.buffer;
                    //@ts-ignore
                    let ext: any = req.file.mimetype;
                    //@ts-ignore
                    let name: any = req.file.originalname;

                    asset_id = await db.AssetDataService.updateAsset(req.scenario_id, req.element_id, name, ext, Buffer.from(buffer).toString('base64'));
                    item.data.asset_id = asset_id;
                    found = true;
                }
            }
            if (!found) {
                return res.json({ ok: false, error: 'element_not_found' });
            }
            let { data } = translateVisualToDataScenario(scenario_visual);
            await db.ScenarioDataService.setScenario(req.scenario_id, data, JSON.stringify(scenario_visual));
            return res.json({ ok: true, asset_id });
        } catch (e) {
            logException(Topic.Api, Severity.Error, e);
            res.json({ ok: false, error: 'unknown' });
        }
    });

    apiHelper.add('/api/protected/api/get_asset', 'get', {
        example: { 'mcn-account-id': 0, asset_id: 1, is_base64: false },
        body_params: [],
        header_params: [{ name: 'mcn-account-id', type: 'number', required: true, description: 'Номер ЛС' }],
        query_params: [
            { name: 'asset_id', type: 'number', required: true, description: 'Номер статического элемента' },
            { name: 'isBase64', type: 'string', required: false, description: 'Формат возвращаемого статического элемент' }
        ],
        checks: []
    }, {
        response: {
            ok: { '$ref': '#/components/property/ok' },
        },
        description: 'Получить ассет по его id',
        summary: 'Получить ассет', tags: ['asset']
    }, async (req, res) => {
        try {
            const asset = await db.AssetDataService.getAsset(req['mcn-account-id'], req.asset_id);

            if (asset) {
                if (req.is_base64) {
                    return res.json(asset.value);
                }

                res.setHeader('Content-Type', asset.ext);
                return res.end(Buffer.from(asset.value, 'base64'));
            } else {
                return res.json([]);
            }
        } catch (e) {
            logException(Topic.Api, Severity.Error, e);
            res.json({ ok: false, error: 'unknown' });
        }
    });

    apiHelper.add('/api/protected/api/get_asset/:asset_id', 'get', {
        example: { 'mcn-account-id': 0, asset_id: 1 },
        body_params: [],
        header_params: [{ name: 'mcn-account-id', type: 'number', required: true, description: 'Номер ЛС' }],
        query_params: [],
        checks: []
    }, {
        response: {
            ok: { '$ref': '#/components/property/ok' },
        },
        description: 'Получить ассет по его id',
        summary: 'Получить ассет', tags: ['asset']
    }, async (req, res) => {
        try {
            const asset = await db.AssetDataService.getAsset(req['mcn-account-id'], req.asset_id);

            if (asset) {
                res.setHeader('Content-Type', asset.ext);
                return res.end(Buffer.from(asset.value, 'base64'));
            } else {
                return res.json([]);
            }
        } catch (e) {
            logException(Topic.Api, Severity.Error, e);
            res.json({ ok: false, error: 'unknown' });
        }
    });
}
