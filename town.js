var arrayTown = [];
function clickButton() {
    var town  = {};
    town.townName = document.getElementById('townName').value;
    town.state = document.getElementById('state').value;
    town.population = document.getElementById('population').value;
    town.population = parseInt(town.population);
    if (isNaN(town.population) || town.population < 0) {
        alert("Население города введено неверно")
        return false; 
    }
    arrayTown.push(town);
    return showTowns();
} 

function compareTowns(townA, townB) {
    return townA.population - townB.population;
}

function showTowns () {
    arrayTown.sort(compareTowns);
    arrayTown.reverse();
    document.getElementById('townTable').innerHTML = '<tr>' +'<th>Название</th>' +'<th>Страна</th>' + '<th>Население</th>' +'</tr>';
    for (i = 0; i < arrayTown.length; i++ ) {
        var str=" ";
        str += '<td>' + arrayTown[i].townName + '</td>';
        str += '<td>' + arrayTown[i].state + '</td>';
        str += '<td>' + arrayTown[i].population + '</td>';
        str += '</tr>';
        document.getElementById('townTable').innerHTML += str;
    }
}