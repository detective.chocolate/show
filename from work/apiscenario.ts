import { ScenarioDBInterface, SecureDBInterface, AssetDBInterface, BackupDBInterface, TemplateDBInterface } from '../../db/interface';
import { ApiHelper } from 'typescript-express-openapi';
import { logException, Topic, SubTopic, Severity } from '../../log';
import { ProtectedScenarioMethods } from '../../action';
import customError from '../../customError';
import { HttpErrorsValues } from '../../constants';

export default function addProtectedScenarioMethods(apiHelper: ApiHelper, db: ScenarioDBInterface & SecureDBInterface & AssetDBInterface & BackupDBInterface & TemplateDBInterface) {
    apiHelper.add('/api/protected/api/get_scenario_scheme', 'post', {
        example: { 'mcn-account-id': 0, scenario_id: 0 },
        body_params: [{ name: 'scenario_id', type: 'number', required: true, description: 'id сценария' }],
        header_params: [{ name: 'mcn-account-id', type: 'number', required: true, description: 'Номер ЛС' }],
        checks: [],
    }, {
        response: {
            ok: { '$ref': '#/components/property/ok' },
            visual_data: { type: 'array', items: { type: 'object' }, description: 'Массив объектов описывающие визуальное строение графа' }
        },
        description: `Получение визуальной схемы сценария - массива элементов двух типов. Вершин и дуг графа, который составляет схему.
                      Подробнее про визуальную схему в xwiki.`,
        summary: 'Получение визуальной схемы сценария', tags: ['scenario']
    }, async (params, res) => {
        try {
            if (!await db.SecureDataService.checkOwnershipScenario(params['mcn-account-id'], params.scenario_id)) {
                throw new customError(HttpErrorsValues.auth);
            }
            res.json({ ok: true, visual_data: JSON.parse(await db.ScenarioDataService.getVisualScenario(params.scenario_id)) });
        } catch (e) {
            logException(Topic.Api, Severity.Error, e);
            e.message === HttpErrorsValues.auth ? res.json({ ok: false, error: HttpErrorsValues.auth }) : res.json({ ok: false, error: HttpErrorsValues.unknown });
        }
    });

    apiHelper.add('/api/protected/api/set_scenario_scheme', 'post', {
        example: { 'mcn-account-id': 0, scenario_id: 0, visual: 'data' },
        body_params: [{ name: 'scenario_id', type: 'number', required: true, description: 'id сценария' },
            {
                name: 'visual', type: 'any', required: true,
                description: 'Список объектов, описывающых визуальное строение графа(ReactFlow)'
            }],
        header_params: [{ name: 'mcn-account-id', type: 'number', required: true, description: 'Номер ЛС' }],
        checks: [],
    }, {
        response: {
            ok: { '$ref': '#/components/property/ok' },
        },
        description: `Изменение сценария.
                      Подробнее про сценарий в xwiki.`,
        summary: 'Изменение сценария', tags: ['scenario']
    }, async (req, res) => {
        try {
            if (! await db.SecureDataService.checkOwnershipScenario(req['mcn-account-id'], req.scenario_id)) {
                throw new customError(HttpErrorsValues.auth);
            }

            let { visual: visualInput, scenario_id }: any = req;
            let mcn_account_id = req['mcn-account-id'];

            let result = await ProtectedScenarioMethods.setScenarioScheme(db, { visualInput, scenario_id, mcn_account_id });

            return res.json(result);
        } catch (e) {
            logException(Topic.Api, Severity.Error, e);

            if (e.message === HttpErrorsValues.auth) {
                res.json({ ok: false, error: HttpErrorsValues.auth });
            } else if (e.message === HttpErrorsValues.invalidVisualScheme) {
                res.json({ ok: false, error: HttpErrorsValues.invalidVisualScheme });
            } else if (e.message === HttpErrorsValues.maxScenarioCount) {
                res.json({ ok: false, error: HttpErrorsValues.maxScenarioCount });
            } else {
                res.json({ ok: false, error: HttpErrorsValues.unknown });
            }
        }
    });

    apiHelper.add('/api/protected/api/get_scenarios', 'post', {
        example: { 'mcn-account-id': 0 },
        body_params: [],
        header_params: [{ name: 'mcn-account-id', type: 'number', required: true, description: 'Номер ЛС' }],
        checks: []
    }, {
        response: {
            ok: { '$ref': '#/components/property/ok' },
        },
        description: `Получение всех сценариев списком. Для каждого сценария будет также информация о интеграциях и коннекторах
                       в подмасивах.`,
        summary: 'Получение всех сценариев списком', tags: ['scenario']
    }, async (req, res) => {
        try {
            res.json({ ok: true, scenarios: await db.ScenarioDataService.getScenarios(req['mcn-account-id']) });
        } catch (e) {
            logException(Topic.Api, Severity.Error, e);
            res.json({ ok: false, error: 'unknown' });
        }
    });


    apiHelper.add('/api/protected/api/create_scenario', 'post', {
        example: { 'mcn-account-id': 0 },
        body_params: [],
        header_params: [{ name: 'mcn-account-id', type: 'number', required: true, description: 'Номер ЛС' }],
        checks: []
    }, {
        response: {
            ok: { '$ref': '#/components/property/ok' },
        },
        description: 'Создание нового сценария',
        summary: 'Создание нового сценария', tags: ['scenario']
    }, async (req, res) => {
        try {
            console.log("API CALLED");
            let mcn_account_id = req['mcn-account-id'];

            let result = await ProtectedScenarioMethods.createScenario(db, { mcn_account_id });
            return res.json(result);
        } catch (e) {
            logException(Topic.Api, Severity.Error, e);
            e.message === HttpErrorsValues.maxScenarioCount ? res.json({ ok: false, error: HttpErrorsValues.maxScenarioCount }) : res.json({ ok: false, error: HttpErrorsValues.unknown });
        }
    });

    apiHelper.add('/api/protected/api/delete_scenario', 'post', {
        example: { 'mcn-account-id': 0, scenario_id: 0 },
        body_params: [{ name: 'scenario_id', type: 'number', required: true, description: 'Номер сценария, который нужно удалить' }],
        header_params: [{ name: 'mcn-account-id', type: 'number', required: true, description: 'Номер ЛС' }],
        checks: []
    }, {
        response: {
            ok: { '$ref': '#/components/property/ok' },
        },
        description: 'Удаление сценария',
        summary: 'Удаление сценария', tags: ['scenario']
    }, async (req, res) => {
        try {
            if (!await db.SecureDataService.checkOwnershipScenario(req['mcn-account-id'], req.scenario_id)) {
                return res.json({ ok: false, error: 'auth' });
            }
            await db.ScenarioDataService.deleteScenario(req.scenario_id);
            res.json({ ok: true });
        } catch (e) {
            logException(Topic.Api, Severity.Error, e);
            res.json({ ok: false, error: 'unknown' });
        }
    });

    apiHelper.add('/api/protected/api/edit_scenario', 'post', {
        example: { 'mcn-account-id': 0, scenario_id: 0, name: 'Новое имя' },
        body_params: [{ name: 'scenario_id', type: 'number', required: true, description: 'Номер сценария, который нужно редактировать' },
            { name: 'name', type: 'string', required: true, description: 'Новое имя для сценария' }],
        header_params: [{ name: 'mcn-account-id', type: 'number', required: true, description: 'Номер ЛС' }],
        checks: []
    }, {
        response: {
            ok: { '$ref': '#/components/property/ok' },
        },
        description: 'Редактирование сценария',
        summary: 'Редактирование сценария', tags: ['scenario']
    }, async (req, res) => {
        try {
            if (! await db.SecureDataService.checkOwnershipScenario(req['mcn-account-id'], req.scenario_id)) {
                return res.json({ ok: false, error: 'auth' });
            }
            await db.ScenarioDataService.editScenario(req.scenario_id, req.name);
            res.json({ ok: true });
        } catch (e) {
            logException(Topic.Api, Severity.Error, e);
            res.json({ ok: false, error: 'unknown' });
        }

    });

    apiHelper.add('/api/protected/api/copy_scenario', 'post', {
        example: { 'mcn-account-id': 0, scenario_id: 0, is_template: false, is_global: false },
        body_params: [{ name: 'scenario_id', type: 'number', required: true, description: 'Номер сценария, который нужно скопировать' },
            { name: 'is_template', type: 'boolean', required: false, description: 'Является ли сценарий шаблоном' },
            { name: 'is_global', type: 'boolean', required: false, description: 'Является ли сценарий глобальным шаблоном' }
        ],
        header_params: [{ name: 'mcn-account-id', type: 'number', required: true, description: 'Номер ЛС' }],
        checks: []
    }, {
        response: {
            ok: { '$ref': '#/components/property/ok' },
            scenario_id: { type: 'number', description: 'id нового сценария, если он был создан' }
        },
        description: 'Копирование сценария',
        summary: 'Копирование сценария', tags: ['scenario']
    }, async (req, res) => {
        try {
            let mcn_account_id = req['mcn-account-id'];
            let { scenario_id, is_template, is_global } = req;

            if (!is_global) is_global = false;
            if (!is_template) is_template = false;
            if (is_global == true) is_template = true;
            if (is_template == true) is_global = true;

            if (!await db.SecureDataService.checkOwnershipScenario(mcn_account_id, scenario_id)) {
                throw new customError(HttpErrorsValues.auth);
            }

            let result = await ProtectedScenarioMethods.copyScenario(db, { mcn_account_id, scenario_id, is_global, is_template });

            return res.json(result);
        } catch (e) {
            logException(Topic.Api, Severity.Error, e);
            if (e.message === HttpErrorsValues.auth) {
                res.json({ ok: false, error: HttpErrorsValues.auth });
            } else if (e.message === HttpErrorsValues.maxScenarioCount) {
                res.json({ ok: false, error: HttpErrorsValues.maxScenarioCount });
            } else {
                res.json({ ok: false, error: HttpErrorsValues.unknown });
            }
        }
    });

    apiHelper.add('/api/protected/api/get_scenario_backup', 'post', {
        example: { 'mcn-account-id': 0, scenario_id: 0 },
        body_params: [{ name: 'scenario_id', type: 'number', required: true, description: 'Номер сценария, по которому получаем бэкапы' }],
        header_params: [{ name: 'mcn-account-id', type: 'number', required: true, description: 'Номер ЛС' }],
        checks: []
    }, {
        response: {
            ok: { '$ref': '#/components/property/ok' },
        },
        description: 'Получение списка всех бэкапов по id сценария',
        summary: 'Получить список бэкапов', tags: ['scenario']
    }, async (req, res) => {
        try {
            if (!await db.SecureDataService.checkOwnershipScenario(req['mcn-account-id'], req.scenario_id)) {
                return res.json({ ok: false, error: 'auth' });
            }

            let backups = await db.BackupDataService.getBackupsByScenarioId(req.scenario_id);

            res.json({ ok: true, backups });
        } catch (e) {
            logException(Topic.Api, Severity.Error, e);
            res.json({ ok: false, error: 'unknown' });
        }
    });
}

