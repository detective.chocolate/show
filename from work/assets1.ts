import { ScenarioDBInterface, SecureDBInterface, AssetDBInterface } from '../../../db/interface';
import { ApiHelper } from 'typescript-express-openapi';;
import { logException, Topic, SubTopic, Severity } from '../../../log';
import { backupRelevanceMinutes, scenarioElementArcUniqueProperties, HttpErrorsValues } from '../../../constants';
import { deepClone, scenarioHealthcheck, changeAssetIds } from '../../../utils';
import translateVisualToDataScenario from '../../../scenario';
import { createScenarioTemplate } from '../createScenarioTemplate';
import customError from '../../../customError';
import { json } from 'express';
import { StringDecoder } from 'string_decoder';
const decoder = new StringDecoder('utf-8')

async function setAsset(db: ScenarioDBInterface & SecureDBInterface & AssetDBInterface, params: {mcn_account_id: number, scenario_id: number, element_id:string, file:any}) {
    console.log('used setAsset')
    let scenario_visual = JSON.parse(await db.ScenarioDataService.getVisualScenario(params.scenario_id));
    if (!Array.isArray(scenario_visual)) {
        throw new customError(HttpErrorsValues.badscenario);;
    }
    let found = false;
    let asset_id;
    for (let i in scenario_visual) {
        const item = scenario_visual[i];
        if (item.id == params.element_id) {
            //@ts-ignore
            let buffer: any = params.file.buffer;
            //@ts-ignore
            let ext: any = params.file.mimetype;
            //@ts-ignore
            let name: any = params.file.originalname;

            asset_id = await db.AssetDataService.updateAsset(params.scenario_id, params.element_id, name, ext, Buffer.from(buffer).toString('base64'));
            item.data.asset_id = asset_id;
            found = true;
        }
    }
    if (!found) {
        return { ok: false, error: 'element_not_found' };
    }
    let { data } = translateVisualToDataScenario(scenario_visual);
    await db.ScenarioDataService.setScenario(params.scenario_id, data, JSON.stringify(scenario_visual));
    return { ok: true, asset_id };
}

async function getAsset(db: ScenarioDBInterface & SecureDBInterface & AssetDBInterface, params:{mcn_account_id:number, asset_id:number,is_base64:boolean, asset:any}) {
    const asset = await db.AssetDataService.getAsset(params.mcn_account_id, params.asset_id);
        if (params.is_base64) {
            let result = decoder.write(Buffer.from(asset.value,'base64'))
            return Object(result);
        }
        console.log(asset.ext)
        //asset.setHeader('Content-Type', asset.ext)
        let result = decoder.write(Buffer.from(asset.value,'base64'))
        //console.log(result)
        return Object(result);
    } 

export {setAsset, getAsset};


//     console.log('hello')
            //     if (req.is_base64) {
            //         return res.json(asset.value);
            //     }
            //     console.log(asset.ext)
            //     res.setHeader('Content-Type', asset.ext);
            //     return res.end(Buffer.from(asset.value, 'base64'));
            // } else {
                //return res.json([]);
