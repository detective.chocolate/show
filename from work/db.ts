export default {
  AssetDataService: {
    async updateAsset(scenario_id: number, element_id: string, name: string, ext: string, value: string) {
      return Promise.resolve(5)
    },
    async deleteUnusedAssets(scenario_id: number, used_assets: Map<string, number>) {
      return Promise.resolve();
    },
    async createTemplateAssets(assets: any, scenario_id: number, new_scenario_id: number) {
      return Promise.resolve([]);
    },
    async copyTemplateAssets(template_id: number, scenario_id: number) {
      return Promise.resolve();
    },
    async getAsset(account_id: number, asset_id: number) {
      return Promise.resolve()
    },
    async setAsset(account_id:number, scenario_id:number, element_id:number, file:any) {
      return Promise.resolve()
    }
  },
  TemplateDataService: {
    async getTemplates(account_id: number, is_include_backups: boolean, is_global: boolean) {
      return Promise.resolve()
    },
    async getTemplateData(template_id: number) {
      return Promise.resolve()
    },
    async createScenarioTemplate(data: string, visual: string, account_id: number, is_backup: boolean, name: string, is_global: boolean) {
      return Promise.resolve(1)
    },
  },
  ScenarioDataService: {
    async setScenario(id: number, data: string, visual: string) {
      return Promise.resolve(true)
    },
    async getVisualScenario(id: number) {
      return Promise.resolve('true')
    },
    async getScenarios(account_id: number) {
      return Promise.resolve([1, 2])
    },
    async createScenario(account_id: number, data: string, visual: string) {
      return Promise.resolve(5)
    },
    async getMaxScenario(account_id: number) {
      return Promise.resolve(5)
    },
    async deleteScenario(scenario_id: number) {
      return Promise.resolve()
    },
    async editScenario(scenario_id: number, name: string) {
      return Promise.resolve()
    },
    async copyScenario(account_id: number, scenario_id: number) {
      return Promise.resolve({ new_scenario_id: 2, visual_data: '[{ "id": "init", "type": "init" }]' })
    }
  },
  BackupDataService: {
    async checkScenarioBackupExistance(scenario_id: number, backupRelevanceMinutes: number) {
      return Promise.resolve(false)
    },
    async storeBackupData(scenario_id: number, backup_scenario_id: number) {
      return Promise.resolve()
    },
    async getBackupsByScenarioId(scenario_id: number) {
      return Promise.resolve()
    }
  }
}
