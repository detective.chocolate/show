import { ScenarioDBInterface, AssetDBInterface, TemplateDBInterface, BackupDBInterface } from '../../../db/interface';
import { backupRelevanceMinutes, scenarioElementArcUniqueProperties, HttpErrorsValues } from '../../../constants';
import { deepClone, scenarioHealthcheck, changeAssetIds } from '../../../utils';
import translateVisualToDataScenario from '../../../scenario';
import { createScenarioTemplate } from '../createScenarioTemplate';
import customError from '../../../customError';

async function setScenarioScheme(db: ScenarioDBInterface & AssetDBInterface & BackupDBInterface & TemplateDBInterface, params: { visualInput: string, mcn_account_id: number, scenario_id: number }) {
    let { visualInput, mcn_account_id, scenario_id } = params;

    if (!Array.isArray(visualInput)) {
        throw new customError(HttpErrorsValues.invalidVisualScheme);
    }

    let scenarioBackupExistance = await db.BackupDataService.checkScenarioBackupExistance(scenario_id, backupRelevanceMinutes);
    if (!scenarioBackupExistance) {
        let visualInputCopy = [];
        for (let i = 0; i < visualInput.length; i++) {
            visualInputCopy.push(deepClone(visualInput[i]));
        }

        let templateId = await createScenarioTemplate(db, visualInputCopy, scenario_id, mcn_account_id, true);

        await db.BackupDataService.storeBackupData(scenario_id, templateId);
    }

    let { data, assets, visual } = translateVisualToDataScenario(visualInput);

    let parsedAssetsArray: any = [];
    for (let asset of assets.keys()) {
        parsedAssetsArray.push({ asset_id: assets.get(asset) });
    }

    await db.AssetDataService.deleteUnusedAssets(scenario_id, parsedAssetsArray);
    return { ok: await db.ScenarioDataService.setScenario(scenario_id, data, JSON.stringify(visual)) };
}


async function createScenario(db: ScenarioDBInterface, params: { mcn_account_id: number }) {
    let { mcn_account_id } = params;
    console.log('!!')
    let max = await db.ScenarioDataService.getMaxScenario(mcn_account_id);
    let current = (await db.ScenarioDataService.getScenarios(mcn_account_id)).length;
    const newScenario = scenarioHealthcheck([], true);
    let { data, visual } = translateVisualToDataScenario(newScenario);
    if (current >= max) {
        throw new customError(HttpErrorsValues.maxScenarioCount);
    }
    return { ok: true, scenario_id: await db.ScenarioDataService.createScenario(mcn_account_id, data, JSON.stringify(visual)) };
}

async function copyScenario(db: ScenarioDBInterface & AssetDBInterface, params: { mcn_account_id: number, scenario_id: number, is_global: boolean, is_template: boolean }) {
    let { mcn_account_id, scenario_id, is_global, is_template } = params;

    let max = await db.ScenarioDataService.getMaxScenario(mcn_account_id);
    let current = (await db.ScenarioDataService.getScenarios(mcn_account_id)).length;
    if (current >= max && !is_global) {
        throw new customError(HttpErrorsValues.maxScenarioCount);
    }

    let { new_scenario_id, visual_data } = await db.ScenarioDataService.copyScenario(mcn_account_id, scenario_id, is_global, is_template);
    visual_data = JSON.parse(visual_data);
    let { assets } = translateVisualToDataScenario(visual_data);
    const createdAssetsIds = await db.AssetDataService.createTemplateAssets(assets, scenario_id, new_scenario_id);
    changeAssetIds(createdAssetsIds, visual_data);

    let updatedScenarioData = translateVisualToDataScenario(visual_data);

    await db.AssetDataService.deleteUnusedAssets(new_scenario_id, createdAssetsIds);
    await db.ScenarioDataService.setScenario(new_scenario_id, updatedScenarioData.data, JSON.stringify(visual_data));
    return { ok: true, scenario_id: new_scenario_id };
}

export { setScenarioScheme, createScenario, copyScenario };

