import * as assert from 'assert';
import { ProtectedAssetMethods, ProtectedScenarioMethods } from '../../src/action';
import db from '../db'



describe('Create_scenario', function () {
    describe('#createScenario()', function () {
      it('Creates copy new scenario and add init element to it', async () => {
        db.ScenarioDataService.createScenario = async (account_id: number, data: string, visual: string) => {
          assert.deepStrictEqual(JSON.parse(visual), [{
            id: 'init',
            type: 'init',
            position: {
              x: 100,
              y: 100
            },
            data: {
              text: 'Здравствуйте'
            }
          }])
          return Promise.resolve(5)
        }
        const response = await ProtectedScenarioMethods.createScenario(db, { mcn_account_id: 1 });
        assert.strictEqual(response.scenario_id, 5);
      });
    });
})

describe('Set scenario scheme', function () {
  describe('#setScenarioScheme()', function () {
    it('Should return scenario copy with new ids on elements, backup not exists', async () => {
      let idx = 0;
      const visualInput: any = [{
        id: "init",
        type: "init"
      }, {
        id: "first",
        type: "execution"
        },]
      db.BackupDataService.checkScenarioBackupExistance = async (scenario_id: number, backupRelevanceMinutes: number) => {
        return Promise.resolve(false)
      }
      db.ScenarioDataService.setScenario = async (id: number, data: string, visual: string) => {

        let parsedVisual = JSON.parse(visual);
        if (idx < 1) {
          //const newScenario = parsedVisual.length === visualInput.length && parsedVisual[parsedVisual.length - 1].source === parsedVisual[1]['id'] && parsedVisual[parsedVisual.length - 1].target === parsedVisual[2]['id'] && parsedVisual[0]['id'] === 'init';
          const newScenario_2 = parsedVisual[1]['type'] === visualInput[1]['type'];
          const isEqual = assert.strictEqual(true, newScenario_2);
          idx += 1;
          return Promise.resolve(Boolean(isEqual));
        } else {
          return Promise.resolve(true)
        }

      }
      const response = await ProtectedScenarioMethods.setScenarioScheme(db, { visualInput, mcn_account_id: 1, scenario_id: 1 });
      assert.deepStrictEqual(response.ok, true);
    });
  });
})



// describe('Assets', function(){
//   describe('#setAsset()', function(){
//     it('Should return new asset', async() =>{


//       db.AssetDataService.setAsset = async(account_id:number,scenario_id:number,element_id:number,file:any) =>{
//         const fil = new Blob(obj,null,2)

//         assert.deepStrictEqual(JSON.parse(file),  

// //       }
// //       const obj = {test:'helo'}
//       //const file = new Blob(obj,null,2)
// //       const response =  await ProtectedAssetMethods.setAsset(db,{mcn_account_id:1,scenario_id:1,element_id:1,file})
// //       assert.deepStrictEqual(response.ok,true)

// //     })
// //   })
// })


