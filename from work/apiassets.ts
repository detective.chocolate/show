import * as assert from 'assert';
import { ProtectedAssetMethods, ProtectedScenarioMethods } from '../../src/import { ScenarioDBInterface, SecureDBInterface, AssetDBInterface } from '../../db/interface';
import { ApiHelper } from 'typescript-express-openapi';
import translateVisualToDataScenario from '../../scenario';
import { logException, Topic, SubTopic, Severity } from '../../log';
import {ProtectedAssetMethods} from  '../../action';
import { json } from 'express';
import { StringDecoder } from 'string_decoder'
const decoder = new StringDecoder('utf8');


export default function addProtectedAssetsMethods(apiHelper: ApiHelper, db: ScenarioDBInterface & SecureDBInterface & AssetDBInterface) {

    apiHelper.add('/api/protected/api/set_asset', 'post', {
        example: { 'mcn-account-id': 0, scenario_id: 0, element_id: 'some_element_id', file: 'file' },
        body_params: [],
        header_params: [{ name: 'mcn-account-id', type: 'number', required: true, description: 'Номер ЛС' }],
        query_params: [{ name: 'scenario_id', type: 'number', required: true, description: 'Номер сценария к которому будет прикреплена статика' },
            { name: 'element_id', type: 'string', required: true, description: 'Идентификатор элемента сценария, к которому будет прикреплена статика' }],
        is_file_upload: true,
        checks: []
    }, {
        response: {
            ok: { '$ref': '#/components/property/ok' },
        },
        description: 'Статика прикрепляется к конкретному узлу сценария.',
        summary: 'Создать статику', tags: ['asset']
    }, async (req, res) => {
        try {
            if (!await db.SecureDataService.checkOwnershipScenario(req['mcn-account-id'], req.scenario_id)) {
                return res.json({ ok: false, error: 'auth' });
            }
            let mcn_account_id = req['mcn-account-id'];
            let scenario_id = req['scenario_id'];
            let element_id = req['element_id'];
            let file = req['file']

            let result = await ProtectedAssetMethods.setAsset(db, {mcn_account_id,scenario_id,element_id,file});


            return res.json(result);
        } catch (e) {
            logException(Topic.Api, Severity.Error, e);
            res.json({ ok: false, error: 'unknown' });
        }
    });

    apiHelper.add('/api/protected/api/get_asset', 'get', {
        example: { 'mcn-account-id': 0, asset_id: 1, is_base64: false },
        body_params: [],
        header_params: [{ name: 'mcn-account-id', type: 'number', required: true, description: 'Номер ЛС' }],
        query_params: [
            { name: 'asset_id', type: 'number', required: true, description: 'Номер статического элемента' },
            { name: 'isBase64', type: 'string', required: false, description: 'Формат возвращаемого статического элемент' }
        ],
        checks: []
    }, {
        response: {
            ok: { '$ref': '#/components/property/ok' },
        },
        description: 'Получить ассет по его id',
        summary: 'Получить ассет', tags: ['asset']
    }, async (req, res) => {
        try {
            const asset = await db.AssetDataService.getAsset(req['mcn-account-id'], req.asset_id);

            let asset_id = req['asset_id'];
            let mcn_account_id = req['mcn-account-id'];
            let is_base64 = req['is_base64'];
            if (asset) {
                let result = await ProtectedAssetMethods.getAsset(db, {mcn_account_id, asset_id, is_base64,asset});
                return res.json(result)
            } else {
                return res.json([])
            }
        } catch (e) {
            logException(Topic.Api, Severity.Error, e);
            res.json({ ok: false, error: 'unknown' });
        }
    });

    apiHelper.add('/api/protected/api/get_asset/:asset_id', 'get', {
        example: { 'mcn-account-id': 0, asset_id: 1 },
        body_params: [],
        header_params: [{ name: 'mcn-account-id', type: 'number', required: true, description: 'Номер ЛС' }],
        query_params: [],
        checks: []
    }, {
        response: {
            ok: { '$ref': '#/components/property/ok' },
        },
        description: 'Получить ассет по его id',
        summary: 'Получить ассет', tags: ['asset']
    }, async (req, res) => {
        try {
            const asset = await db.AssetDataService.getAsset(req['mcn-account-id'], req.asset_id);

            if (asset) {
                res.setHeader('Content-Type', asset.ext);
                return res.end(Buffer.from(asset.value, 'base64'));
            } else {
                return res.json([]);
            }
        } catch (e) {
            logException(Topic.Api, Severity.Error, e);
            res.json({ ok: false, error: 'unknown' });
        }
    });
}


