var xhttp = new XMLHttpRequest();

xhttp.onreadystatechange = function(){
    if (this.readyState === 4 && this.status === 200) {
        processLangDocument(this.responseText);
    } 
};

function request (file)
{
    xhttp.open("GET", file, true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
};

function setCookie(cname,cvalue,exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0) {
            alert (c);
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie() {
    var user=getCookie("username");
    if (user != "") {
        alert("Добро пожаловать снова,  " + user);
    } else {
        user = prompt("Пожалуйста, представьтесь: ","");
        if (user != "" && user != null) {
            setCookie("username", user, 30);
        }
    }
}
