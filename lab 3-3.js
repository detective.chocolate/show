var arrayTown = [];
while (confirm('Вы хотите ввести город?'))
{
    var town = {};
    town.townName = prompt ('Укажите название');
    town.state = prompt('Укажите страну');
    town.population = prompt('Укажите насселение');
    town.population = parseInt(town.population);
    if (isNaN(town.population) || town.population < 0) {
        alert("Население города введено неверно")
        return false;
    }
    arrayTown.push(town);
}

function compareTowns(townA, townB) {
    return townA.population - townB.population;
}

arrayTown.sort(compareTowns);

alert('Самый населенный город ' + arrayTown[arrayTown.length - 1].townName + ' с населением в ' + arrayTown[arrayTown.length - 1].population + ' человек');
