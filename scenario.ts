import { scenarioHealthcheck } from '../../src/utils';
import { ProtectedScenarioMethods } from "../../src/action/methods";
import * as assert from 'assert';
import db from '../db'
import { HttpErrorsValues } from '../../src/constants'

describe('scenario-healthcheck', function () {
  describe('#scenarioHealthcheck()', function () {
    it('Scenario has more then 1 init element without ids and is backup', () => {
      const array = [
        { type: 'init' },
        { type: 'init' }
      ];
      const newArray = [
        { type: 'init', id: 'init' }
      ]
      assert.deepStrictEqual(scenarioHealthcheck(array, true), newArray);
    });
    it('Scenario has less then 1 init element', () => {
      const array = [
        { type: 'randomElement' }
      ];
      const newArray = [
        { type: 'randomElement' }, {
          id: 'init',
          type: 'init',
          position: {
            x: 100,
            y: 100
          },
          data: {
            text: 'Здравствуйте'
          }
        }
      ]
      assert.deepStrictEqual(scenarioHealthcheck(array, true), newArray);
    });
    it('Scenario has element with extra arc goint into it', () => {
      const array = [
        { type: 'init', id: 'init' },
        { type: 'random', id: 'second' },
        { source: 'ldfkgdfg', target: 'first' }
      ];
      const newArray = [
        { type: 'init', id: 'init' },
        { type: 'random', id: 'second' },
      ]
      assert.deepStrictEqual(scenarioHealthcheck(array, false), newArray);
    });
    it('Scenario has element with extra arc outgoing from it and broken init and is backup', () => {
      const array = [
        { type: 'init', id: 'first' },
        { type: 'random', id: 'second' },
        { source: 'random', target: 'fldgfdg' }
      ];
      const newArray = [
        { type: 'init', id: 'init' },
        { type: 'random', id: 'second' },
      ]
      assert.deepStrictEqual(scenarioHealthcheck(array, true), newArray);
    });
    it('Scenario has element with extra arc outgoing from it and going into it and broken init and is backup', () => {
      const array = [
        { type: 'init', id: 'first' },
        { type: 'random', id: 'second' },
        { source: 'second', target: 'first' },
        { source: 'random', target: 'second' },
        { source: 'fdfjgdkfg', target: 'init' }
      ];
      const newArray = [
        { type: 'init', id: 'init' },
        { type: 'random', id: 'second' },
      ]
      assert.deepStrictEqual(scenarioHealthcheck(array, true), newArray);
    });
    it('Scenario has 2 init elements (1 is broken) and element with extra arc outgoing from it and going into it and is backup', () => {
      const array = [
        { type: 'init', id: 'init' },
        { type: 'init', id: 'fgdfkgnfdkj' },
        { type: 'random', id: 'second' },
        { type: 'random', id: 'third' },
        { source: 'second', target: 'third' },
        { source: 'fdfjgdkfg', target: 'init' }
      ];
      const newArray = [
        { type: 'init', id: 'init' },
        { type: 'random', id: 'second' },
        { type: 'random', id: 'third' },
        { source: 'second', target: 'third' },
      ]
      assert.deepStrictEqual(scenarioHealthcheck(array, true), newArray);
    });
  });
});

describe('Set scenario scheme', function () {
  describe('#setScenarioScheme()', function () {
    it('Should return scenario copy with new ids on elements, backup not exists', async () => {
      let idx = 0;
      const visualInput: any = [{
        id: "init",
        type: "init"
      }, {
        id: "first",
        type: "any"
      },
      {
        id: "second",
        type: "any",
      }, {
        id: "123fvdvddf",
        source: "first",
        target: "second",
        sourceHandle: "null"
      }];
      db.BackupDataService.checkScenarioBackupExistance = async (scenario_id: number, backupRelevanceMinutes: number) => {
        return Promise.resolve(false)
      }
      db.ScenarioDataService.setScenario = async (id: number, data: string, visual: string) => {

        let parsedVisual = JSON.parse(visual);
        if (idx < 1) {
          const newScenario = parsedVisual.length === visualInput.length && parsedVisual[parsedVisual.length - 1].source === parsedVisual[1]['id'] && parsedVisual[parsedVisual.length - 1].target === parsedVisual[2]['id'] && parsedVisual[0]['id'] === 'init';
          const isEqual = assert.strictEqual(true, newScenario);
          idx += 1;
          return Promise.resolve(Boolean(isEqual));
        } else {
          return Promise.resolve(true)
        }

      }
      const res = await ProtectedScenarioMethods.setScenarioScheme(db, { visualInput, mcn_account_id: 1, scenario_id: 1 });
      assert.deepStrictEqual(res.ok, true);
    });
    it('Should return scenario with init element (create it), backup dont exists', async () => {
      const visualInput: any = [];
      let idx = 0;
      db.BackupDataService.checkScenarioBackupExistance = async (scenario_id: number, backupRelevanceMinutes: number) => {
        return Promise.resolve(false)
      }
      db.ScenarioDataService.setScenario = async (id: number, data: string, visual: string) => {
        if (idx < 1) {
          const newScenario = [{
            id: 'init',
            type: 'init',
            position: {
              x: 100,
              y: 100
            },
            data: {
              text: 'Здравствуйте'
            }
          }]
          const isEqual = assert.deepStrictEqual(JSON.parse(visual), newScenario);
          idx += 1;
          return Promise.resolve(Boolean(isEqual));
        } else {
          return Promise.resolve(true)
        }
      }
      const res = await ProtectedScenarioMethods.setScenarioScheme(db, { visualInput, mcn_account_id: 1, scenario_id: 1 });

      assert.deepStrictEqual(res.ok, true);
    });
    it('Should return scenario with init element (create it), backup exists', async () => {
      const visualInput: any = [];
      let idx = 0;
      const backupExistance = true;
      db.BackupDataService.checkScenarioBackupExistance = async (scenario_id: number, backupRelevanceMinutes: number) => {
        return Promise.resolve(backupExistance)
      }
      db.ScenarioDataService.setScenario = async (id: number, data: string, visual: string) => {
        if (idx < 1 && !backupExistance) {
          const newScenario = [{
            id: 'init',
            type: 'init',
            position: {
              x: 100,
              y: 100
            },
            data: {
              text: 'Здравствуйте'
            }
          }]
          const isEqual = assert.deepStrictEqual(JSON.parse(visual), newScenario);
          idx += 1;
          return Promise.resolve(Boolean(isEqual));
        } else {
          return Promise.resolve(true)
        }
      }
      const res = await ProtectedScenarioMethods.setScenarioScheme(db, { visualInput, mcn_account_id: 1, scenario_id: 1 });

      assert.deepStrictEqual(res.ok, true);
    });
  });
})

describe('Copy scenario scheme', function () {
  describe('#copyScenarioScheme()', function () {
    it('Dont create copy of scenario if scenario count equals to maxScenarioCount', async () => {
      let idx = 0;
      const visualInput: any = [{
        id: "init",
        type: "init"
      }, {
        id: "first",
        type: "any"
      },
      {
        id: "second",
        type: "any",
      }, {
        id: "123fvdvddf",
        source: "first",
        target: "second",
        sourceHandle: "null"
      }];
      db.ScenarioDataService.getMaxScenario = async (account_id: number) => {
        return Promise.resolve(2)
      }
      db.ScenarioDataService.getScenarios = async (account_id: number) => {
        return Promise.resolve([1, 2])
      }
      try {
        const res: any = await ProtectedScenarioMethods.copyScenario(db, { mcn_account_id: 1, scenario_id: 1, is_global: false, is_template: false });
      } catch (e) {
        assert.strictEqual(e.message, HttpErrorsValues.maxScenarioCount);
      }
    });
    it('Create copy of scenario (scenario count less then maxScenarioCount)', async () => {
      let idx = 0;
      const visualInput: any = [{
        id: "init",
        type: "init"
      }, {
        id: "first",
        type: "any"
      },
      {
        id: "second",
        type: "any",
      }, {
        id: "123fvdvddf",
        source: "first",
        target: "second",
        sourceHandle: "null"
      }];
      db.ScenarioDataService.getMaxScenario = async (account_id: number) => {
        return Promise.resolve(2)
      }
      db.ScenarioDataService.getScenarios = async (account_id: number) => {
        return Promise.resolve([1])
      }
      let res: any;
      try {
        res = await ProtectedScenarioMethods.copyScenario(db, { mcn_account_id: 1, scenario_id: 1, is_global: false, is_template: false });
      } catch (e) {
      }
      assert.strictEqual(res.scenario_id, 2);
    });
  });
})

describe('Create scenario', function () {
  describe('#createScenario()', function () {
    it('Creates copy new scenario and add init element to it', async () => {
      db.ScenarioDataService.createScenario = async (account_id: number, data: string, visual: string) => {
        assert.deepStrictEqual(JSON.parse(visual), [{
          id: 'init',
          type: 'init',
          position: {
            x: 100,
            y: 100
          },
          data: {
            text: 'Здравствуйте'
          }
        }])
        return Promise.resolve(5)
      }
      const response = await ProtectedScenarioMethods.createScenario(db, { mcn_account_id: 1 });
      assert.strictEqual(response.scenario_id, 5);
    });
  });
})



